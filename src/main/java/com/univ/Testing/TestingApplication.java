package com.univ.Testing;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestingApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(TestingApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		Compte compte = new Compte();
		float solde = compte.getSolde();
		System.out.println("=============================================");
		System.out.println("solde = " + solde);
		System.out.println("=============================================");
	}
}
